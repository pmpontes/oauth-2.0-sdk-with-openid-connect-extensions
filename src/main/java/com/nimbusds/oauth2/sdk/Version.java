package com.nimbusds.oauth2.sdk;


/**
 * Version information about this OAuth 2.0 SDK.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-28)
 */
public class Version {

	
	/**
	 * This SDK version.
	 */
	public static final String SDK_VERSION = "$version$";
	
	
	/**
	 * Prevents instantiation.
	 */
	private Version() {
	
		// Nothing to do
	}
}
