package com.nimbusds.oauth2.sdk;


/**
 * Marker interface for OAuth 2.0 and related protocol messages.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-28)
 */
public interface Message { }
