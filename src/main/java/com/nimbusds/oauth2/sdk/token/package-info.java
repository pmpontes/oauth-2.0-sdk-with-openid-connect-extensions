/**
 * OAuth 2.0 access and refresh token implementations.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-18)
 */
package com.nimbusds.oauth2.sdk.token;