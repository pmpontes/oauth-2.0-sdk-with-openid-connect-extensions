/**
 * Common utility classes.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.oauth2.sdk.util;
