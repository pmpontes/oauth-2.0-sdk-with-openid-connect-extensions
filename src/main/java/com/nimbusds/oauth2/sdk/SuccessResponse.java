package com.nimbusds.oauth2.sdk;


/**
 * Response message indicating success.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-28)
 */
public interface SuccessResponse extends Response { }
