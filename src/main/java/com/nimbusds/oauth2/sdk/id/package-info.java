/**
 * Common OAuth 2.0 identifier and identity classes.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-18)
 */
package com.nimbusds.oauth2.sdk.id;