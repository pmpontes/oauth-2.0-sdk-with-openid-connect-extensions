/**
 * OpenID Connect Relying Party (RP) classes.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.sdk.rp;