package com.nimbusds.openid.connect.sdk.claims;


/**
 * Enumeration of the claim requirement types.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-21)
 */
public enum ClaimRequirement {


	/**
	 * Essential claim.
	 */
	ESSENTIAL,


	/**
	 * Voluntary claim.
	 */
	VOLUNTARY
}