/**
 * Classes for representing, serialising and parsing OpenID Connect client 
 * requests and server responses.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.sdk;