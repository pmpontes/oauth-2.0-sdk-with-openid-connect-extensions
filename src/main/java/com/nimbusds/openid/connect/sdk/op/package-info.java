/**
 * OpenID Connect Provider (OP) classes.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-02-11)
 */
package com.nimbusds.openid.connect.sdk.op;