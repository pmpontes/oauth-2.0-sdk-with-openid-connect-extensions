package com.nimbusds.openid.connect.sdk;


import com.nimbusds.oauth2.sdk.Response;


/**
 * OpenID Connect authorisation response.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OpenID Connect Messages 1.0, sections 2.1.2 and 2.1.3.
 * </ul>
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-30)
 */
public interface OIDCAuthorizationResponse extends Response { }
