/**
 * Claims and claim sets used in OpenID Connect.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.openid.connect.sdk.claims;
